var moment = require('moment');

function fixTimestamps(s_unprocessed){

  if (s_unprocessed.length != 100) {
    console.log("Length is " + s_unprocessed.length);
    throw new Error("Array must have exactly 100 packets");
  }
  var previousDatetime = moment('1900-01-01 00:00:00', "YYYY-MM-DD HH:mm:ss");
  var finalTimestamp;

  // First pass
  for (var s in s_unprocessed) {

    var year = s_unprocessed[s].date.year + 2000;
    var month =s_unprocessed[s].date.month;
    var day = s_unprocessed[s].date.day;
    var hours = s_unprocessed[s].date.hour;
    var minutes = s_unprocessed[s].date.minute;
    var seconds = s_unprocessed[s].date.second;
    //var milliseconds = s_unprocessed[s].timestamp - s_initialmillis;

    var datetime = moment(year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds, "YYYY-MM-DD HH:mm:ss");

    if (!datetime.isSame(previousDatetime)) {
    		//document.write("Different<br>");
        previousDatetime = datetime;
        startMillis = s_unprocessed[s].timestamp
        finalTimestamp = datetime;
    } else {
    		finalTimestamp = datetime.add(s_unprocessed[s].timestamp - startMillis, 'ms')
    }
    //console.log(finalTimestamp.format('MMMM Do YYYY, h:mm:ss:SSS') );
    s_unprocessed[s].timestamp = finalTimestamp.valueOf();

  }

  // Second pass
  previousDatetime = moment('1900-01-01 00:00:00', "YYYY-MM-DD HH:mm:ss");

  var ms_counter = 99;
  var changeNext = false;

  for (var i = s_unprocessed.length - 1; i >= 0; --i) {
    if (changeNext) {
        // If the previous millisecond value was zero then set the next one to
        // 99 and keep looping to set previous values to ms_counter - 1
        s_unprocessed[i].timestamp = moment(s_unprocessed[i].timestamp).millisecond(ms_counter).valueOf();
        ms_counter--;
    } else {
        if (moment(s_unprocessed[i].timestamp).get('millisecond') == 0) {
          changeNext = true;
        }
    }
  }
//  console.log(s_unprocessed);
  return s_unprocessed;
}

module.exports = fixTimestamps;

var fixTimestamps = require('./fixTimestamps.js');
var Readable = require('stream').Readable;
var util = require('util');
var Fusion = require("stream-fusion");
var DataSet = require("seeljavascript");
var moment = require('moment');


class ROM {
  constructor() {
    this.firstAddress = null;

    this.s1stream = new Readable({
       objectMode: true,
       read() {}
     })

    this.s2stream = new Readable({
        objectMode: true,
        read() {}
      })

      // get a dataSet object
      this.dataSet = DataSet(1.0 / 100.0);


      this.fused = new Fusion(
        // Need check to be true on one stream so data is emitted
          {stream: this.s1stream, key:"timestamp", check: true},
          {stream: this.s2stream, key: "timestamp", check: true},
          // Options
          //{bufferLength: 1000, buffer: 200}
          //{bufferLength: 5000, buffer: 1}
      );

      this.sameCount = 0;
      this.isCalibrated = false;
      var that = this;
      this.fused.transform = function(streamData) {
        //console.log("Chris " + streamData[0][0].timestamp.toString() + " " + streamData[1][0].timestamp.toString() + " " + streamData[0][0].sensor.toString() + " " + streamData[1][0].sensor.toString());
        //console.log(getRow(streamData[0][0]).toString());

        console.log("CHRIS TS1 " + streamData[0][0].timestamp + " TS2 " + streamData[1][0].timestamp)
        if (streamData[0][0].timestamp == streamData[1][0].timestamp) {
          console.log('Count ' + that.sameCount);
          if (that.sameCount==9000) {
              that.dataSet.Calibrate(that.sameCount);
              that.isCalibrated = true;
              console.log('Calibrated');
          }
          if (that.isCalibrated) {
            //dataSet.Calibrate(sameCount); // CAlibrate each time to get better results
            that.dataSet.CalculateAlpha();
            //console.log(dataSet.alpha_gyr[sameCount] + ' ROM: ' + dataSet.alpha_acc_gyr[sameCount]);
            //console.log(dataSet.alpha_acc_gyr[sameCount-1]);
          //  rn_bridge.channel.send("CHRIS ROM  " + dataSet.alpha_acc_gyr[sameCount-1]);
            console.log("CHRIS ROM  " + that.dataSet.alpha_acc_gyr[this.sameCount-1]);
            //ws.write(sameCount-1 + "," + dataSet.alpha_acc_gyr[sameCount-1] + "\n");

          }
          that.dataSet.AppendRow(that.getRow(streamData[0][0]), that.getRow(streamData[1][0]))
          that.sameCount++;
        }

        // can push as many times as desired but only one item per push
      };
  }

  sendChunk (msg) {
    s1chunk = [];
    s2chunk = [];
    console.log("CHRIS NODE MSG");
    var chunk = JSON.parse(msg);

    if (this.firstAddress==null) {
      this.firstAddress=chunk[0].address
    }

    if (chunk[0].address == this.firstAddress) {

       // Sorting out timestamps so we have ms precision
      s1chunk = fixTimestamps(chunk);

      for (var x in s1chunk) {
        this.s1stream.push(s1chunk[x]);
      }
      s1chunk = [];

    } else {

        // Sorting out timestamps so we have ms precision
       s2chunk = fixTimestamps(chunk);

       for (var x in s2chunk) {
         this.s2stream.push(s2chunk[x]);
       }
       s2chunk = [];
    }
  }

  getRow(item)  {
  //  console.log("CHRIS " + JSON.stringify(item));
    var rowArray = new Array(10);
    rowArray[0] = item.record;
    rowArray[1] = 0; // Type - not used
    rowArray[2] = new moment(item.timestamp);
    rowArray[3] = 0; // Time - not used
    rowArray[4] = item.gyros.x; // x
    rowArray[5] = item.gyros.y; // y
    rowArray[6] = item.gyros.z; // z
    rowArray[7] = item.accels.x; // x
    rowArray[8] = item.accels.y; // y
    rowArray[9] = item.accels.z; // z
    return rowArray;
  }

}

module.exports = ROM;

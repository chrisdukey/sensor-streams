/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */



import Snoopy from 'rn-snoopy'
import EventEmitter from 'EventEmitter'
import filter from 'rn-snoopy/stream/filter'
const emitter = new EventEmitter()
const events = Snoopy.stream(emitter);

// Show calls going from Javascript to Native: create an info shape
// with Snoopy.TO_NATIVE. Use true to log filter results.
//filter({ type: Snoopy.TO_NATIVE }, true)(events).subscribe()
filter({ type: Snoopy.TO_JS }, true)(events).subscribe()

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import RNMultipleBLE from 'rn-multiple-ble'
import { DeviceEventEmitter } from 'react-native'
import ROM from './main.js';
//import hamsters from './hamsters/hamsters.js'
//import { Worker } from 'rn-workers'

var rom = new ROM();


export default class RealmTest extends Component {

  constructor(props) {
    super(props);
    this.state = {info: 0};
    this._onPressButton = this._onPressButton.bind(this)
    this._onAndroidPressButton = this._onAndroidPressButton.bind(this)

    this._onPressConnect = this._onPressConnect.bind(this)
    this._onPressStream = this._onPressStream.bind(this)
    this._onPressStopStreamDisconnect = this._onPressStopStreamDisconnect.bind(this)
  }

  packetArrayListener = (msg) => {
      //console.log("CHRIS Packet " + JSON.parse(msg)[0].address);
      //console.log("CHRIS Packet array message " + fixTimestamps(JSON.parse(msg)).toString())
      rom.sendChunk(msg);

  }

  deviceConnectListener = (msg) => {
      console.log("CHRIS Connected message " + JSON.stringify(msg))
  }

  deviceDisconnectListener = (msg) => {
      console.log("CHRIS Disconnected message " + JSON.stringify(msg))
  }

  componentWillMount() {
    DeviceEventEmitter.addListener('packetArrayListener', this.packetArrayListener);
    DeviceEventEmitter.addListener('deviceConnect', this.deviceConnectListener);
    DeviceEventEmitter.addListener('deviceDisconnect', this.deviceDisconnectListener);

  }

  componentWillUnmount(){
    DeviceEventEmitter.removeListener('packetArrayListener');
    DeviceEventEmitter.removeListener('deviceConnect');
  }

  _onPressButton(){

  }

    _onPressConnect(){
      RNMultipleBLE.connect('18:04:00:DE:00:42')
      RNMultipleBLE.connect('19:04:00:DE:00:42')


    }

    _onPressStream(){
      RNMultipleBLE.setRealtimeClock('19:04:00:DE:00:42').then(RNMultipleBLE.controlStreaming('19:04:00:DE:00:42', true))
    RNMultipleBLE.setRealtimeClock('18:04:00:DE:00:42').then(RNMultipleBLE.controlStreaming('18:04:00:DE:00:42', true))

    }

    _onPressStopStreamDisconnect(){
      RNMultipleBLE.controlStreaming('18:04:00:DE:00:42', false).then(
        RNMultipleBLE.disconnect('18:04:00:DE:00:42'))
        RNMultipleBLE.controlStreaming('19:04:00:DE:00:42', false).then(
          RNMultipleBLE.disconnect('19:04:00:DE:00:42'))
    }


    _onAndroidPressButton(){

  }


  render() {

      return (
        <View style={styles.container}>
          <Text style={styles.welcome}>
            {this.state.info}
          </Text>
          <TouchableOpacity onPress={this._onPressConnect}>
            <Text>Connect</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._onPressStream}>
            <Text>Stream</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._onPressStopStreamDisconnect}>
            <Text>Stop and Disconnect</Text>
          </TouchableOpacity>

        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('RealmTest', () => RealmTest);
